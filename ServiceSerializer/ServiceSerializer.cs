﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;

namespace ServiceSerializer
{
    static  public class ServiceSerializer
    {
        static public T Deserializer<T>(this XElement xElement)
        {
            Encoding encoding = Encoding.GetEncoding("ISO-8859-1");
            using (var memoryStream = new MemoryStream(encoding.GetBytes(xElement.ToString())))
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                return (T)xmlSerializer.Deserialize(memoryStream);
            }
        }

        static public XElement Serializer<T>(this object o)
        {
            Encoding encoding = Encoding.GetEncoding("ISO-8859-1");
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(memoryStream, o);
                    return XElement.Parse(encoding.GetString(memoryStream.ToArray()));
                }
            }
        }
    }
}
